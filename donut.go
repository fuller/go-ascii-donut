package main

import (
	"math"
)

type Donut struct {
	w int
	h int
	z [][]float64
	b [][]rune
}

func (d *Donut) init(width int, height int) {
	// initialize Donut
	d.w = width
	d.h = height

	// initialize arrays
	d.z = make([][]float64, width)
	d.b = make([][]rune, width)

	for i := range d.z {
		d.z[i] = make([]float64, height)
		d.b[i] = make([]rune, height)
	}
}

// refactored following https://www.a1k0n.net/2011/07/20/donut-math.html
func (donut *Donut) calculate_classic(A float64, B float64) {
	var R1 float64 = 1.0   // torus cross-section
	var R2 float64 = 2.0   // torus itself
	var K1x float64 = 0.75 // actually 2*K1
	var K1y float64 = 1.50 //actually 2*K1
	var K2 float64 = 5.0
	var donutChars string = `.,-~:;=!*#$@`

	var dtheta float64 = 0.07
	var dphi float64 = 0.02

	sinA := math.Sin(A)
	cosA := math.Cos(A)
	sinB := math.Sin(B)
	cosB := math.Cos(B)

	charNorm := (float64(len(donutChars)) / math.Sqrt(2.0))

	for theta := 0.0; theta < 2.0*math.Pi; theta += dtheta {

		sinTheta := math.Sin(theta)
		cosTheta := math.Cos(theta)

		circleX := R2 + R1*cosTheta
		circleY := R1 * sinTheta

		for phi := 0.0; phi < 2.0*math.Pi; phi += dphi {
			sinPhi := math.Sin(phi)
			cosPhi := math.Cos(phi)

			coordX := circleX*cosPhi*cosB + circleX*sinPhi*sinA*sinB - circleY*cosA*sinB
			coordY := circleX*cosPhi*sinB - circleX*sinPhi*sinA*cosB + circleY*cosA*cosB

			D := 1.0 / (circleX*sinPhi*cosA + circleY*sinA + K2)

			x := int(float64(donut.w) * (1.0 + K1x*D*coordX) / 2.0)
			y := int(float64(donut.h) * (1.0 - K1y*D*coordY) / 2.0)

			N := (cosPhi*cosTheta*sinB - sinPhi*cosTheta*cosA - sinTheta*sinA + sinTheta*cosA*cosB - sinPhi*cosTheta*sinA*cosB)

			// assign ASCII characters from the provided string
			L := int(charNorm * N)

			if y > 0 && y < donut.h && x > 0 && x < donut.w && D > donut.z[x][y] {
				donut.z[x][y] = D
				if N > 0 {
					donut.b[x][y] = rune(donutChars[L])
				} else {
					donut.b[x][y] = rune(0)
				}
			}
		}
	}

}
