# go-ascii-donut

The ASCII Donut implemented in Go.

## Sources

The ASCII donut was first brought to my attention by Shriraj Hegde via his post on the [Fortran Lang Discourse]().

The donut was originally [written in C by Andy Sloane](https://www.a1k0n.net/2006/09/15/obfuscated-c-donut.html), then later [explained](https://www.a1k0n.net/2011/07/20/donut-math.html), and finally [revised to have no dependence on a math library](https://www.a1k0n.net/2021/01/13/optimizing-donut.html).
While the revised version  with the elimination of floats, sines, cosines, etc. is impressive, I prefer the simpler and more mathematically elegant representation of the original.

Instruction on an approach for terminal animations in Go is derived from [Josh Alletto's Go implementation of Pong](https://earthly.dev/blog/pongo/).
