package main

import (
	"time"

	"github.com/gdamore/tcell/v2"
)

type Animation struct {
	screen tcell.Screen
	donut  Donut
}

func (a *Animation) RunDonut() {

	s := a.screen

	defStyle := tcell.StyleDefault.Background(tcell.ColorBlack).Foreground(tcell.ColorBlack)

	width, height := s.Size()

	var A float64 = 0.0
	var B float64 = 0.0

	for {

		s.Clear()

		// zero out arrays
		a.donut.init(width, height)

		a.donut.calculate_classic(A, B)

		for x := 0; x < width; x++ {
			for y := 0; y < height; y++ {
				s.SetContent(x, y, a.donut.b[x][y], nil, defStyle)
			}
		}

		time.Sleep(15 * time.Millisecond)
		s.Show()

		A += 0.04
		B += 0.02

	}
}
